#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.client import conan_api

requirements = (
    "PocoDNSSD/0.1@noface/stable",
    "PocoDNSSD-Bonjour/0.1@noface/stable",
    "PocoDNSSD-Avahi/0.1@noface/stable",
    "PocoDNSSD-core/0.1@noface/stable",
    "Poco/1.7.9@pocoproject/stable"
)

class DependenciesManager(object):
    def __init__(self):
        self.conan, self.client_cache, self.user_io = conan_api.Conan.factory()

    def install_dependencies(self):
        self.copy_requirements()

        print("conan install . -pr ./dce_profile.txt --build missing")
        self.conan.install(".", profile_name="./dce_profile.txt", build=["missing"])

    def remove_dependencies(self):
        for r in requirements:
            req_dce = self.change_user_channel(r, 'noface/dce')

            if self.exist_recipe(req_dce): #recipe does not exist yet
                print("conan remove -f --builds --packages {}".format(req_dce))
                self.conan.remove(req_dce, force=True)

    def copy_requirements(self):
        for r in requirements:
            req_dce = self.change_user_channel(r, 'noface/dce')

            if not self.exist_recipe(req_dce): #recipe does not exist yet
                print("conan copy {} noface/dce".format(r))
                self.conan.copy(r, "noface/dce")

    def change_user_channel(self, requirement, new_user_channel):
        lib_ver, user_channel = requirement.split('@')

        return lib_ver + "@" + new_user_channel

    def exist_recipe(self, requirement):
        return len(self.conan.search_recipes(requirement)) > 0
