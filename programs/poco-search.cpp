#include "PocoDnssdApp.h"

#include "Poco/Delegate.h"

using namespace std;
using namespace Poco::DNSSD;

class PocoSearchApp: public PocoDnssdApp{
public:
    using super = PocoDnssdApp;

public:
    bool parseArgs(int argc, const char ** argv) override{
        if (argc != 2) {
            cout << "Usage: " << argv[0] << " <serviceType>" << endl;
            cout << endl;
            cout << "Where:" << endl
                 << "\t" "serviceType: type of service to search for (ex.: _http._tcp)" "\n"
                 << endl;
            return false;
        }

        serviceType = std::string(argv[1]);

        return true;
    }

    int run() override{
        try {
            initialize();
            searchService();
            waitFinish();
            uninitialize();
        } catch (const Poco::Exception & e) {
            cerr << e.displayText() << endl;
        }

        return 0;
    }

    void searchService(){
        cout << "Searching for services of type: ";
        cout << serviceType << endl;

        dnssdResponder->browser().serviceFound += Poco::delegate(onServiceFound);
        dnssdResponder->browser().serviceResolved += Poco::delegate(onServiceResolved);
        dnssdResponder->browser().hostResolved += Poco::delegate(onHostResolved);
        dnssdResponder->browser().hostResolveError += Poco::delegate(onHostResolveError);
        dnssdResponder->browser().browseError += Poco::delegate(onError);

        browseHandle = dnssdResponder->browser().browse(serviceType, "");
    }

    void uninitialize() override{
        if (browseHandle.isValid()) {
            cout << "Stopping browse operation" << endl;
            dnssdResponder->browser().cancel(browseHandle);
            browseHandle.reset();
        }

        super::uninitialize();
    }

protected:
    static void onServiceFound(const void* sender, const DNSSDBrowser::ServiceEventArgs& args)
    {
        std::cout << "Service Found: \n"
            << "  Name:      " << args.service.name() << "\n"
            << "  Domain:    " << args.service.domain() << "\n"
            << "  Type:      " << args.service.type() << "\n"
            << "  Interface: " << std::to_string(args.service.networkInterface()) << "\n"
            << "  Port:      " << std::to_string(args.service.port()) << "\n"
            << std::endl;

        DNSSDBrowser* browser = castToBrowser(sender);
        browser->resolve(args.service, 0);
    }

    static void onServiceResolved(const void* sender, const DNSSDBrowser::ServiceEventArgs& args){
        std::cout << "Service Resolved: \n"
            << "  Name:      " << args.service.name() << "\n"
            << "  Full Name: " << args.service.fullName() << "\n"
            << "  Domain:    " << args.service.domain() << "\n"
            << "  Type:      " << args.service.type() << "\n"
            << "  Interface: " << std::to_string(args.service.networkInterface()) << "\n"
            << "  Host:      " << args.service.host() << "\n"
            << "  Port:      " << std::to_string(args.service.port()) << "\n"
            << "  Properties: \n";

        for (const auto & prop : args.service.properties())
        {
            std::cout << "    " << prop.first << ": " << prop.second << "\n";
        }
        std::cout << std::endl;

        DNSSDBrowser * browser = castToBrowser(sender);
        browser->resolveHost(args.service.host(), 0, 0);
    }

    static void onHostResolved(const void * sender, const DNSSDBrowser::ResolveHostEventArgs & args){
        std::cout << "Host resolved: \n"
                  << "  Host:      " << args.host << "\n"
                  << "  IP:        " << args.address.toString() << "\n"
                  << "  Interface: " << std::to_string(args.networkInterface) << "\n"
                  << std::endl;
    }

    static void onError(const void* sender, const DNSSDBrowser::ErrorEventArgs& args)
    {
        std::cerr
            << "Browse failed: "
            << args.error.message()
            << " (" << std::to_string(args.error.code()) << ")"
            << std::endl;
    }

    static void onHostResolveError(const void * sender, const DNSSDBrowser::ErrorEventArgs & args){
        std::cerr
            << "Resolve host failed: "
            << args.error.message()
            << " (" << std::to_string(args.error.code()) << ")"
            << std::endl;
    }

    static DNSSDBrowser * castToBrowser(const void* sender){
        return reinterpret_cast<DNSSDBrowser *>(const_cast<void*>(sender));
    }
protected:
    BrowseHandle browseHandle;

    std::string serviceType;
};

int main(int argc, const char ** argv){
    try{
        PocoSearchApp app;
        app.main(argc, argv);
    }
    catch(const Poco::Exception & e){
        cerr << e.displayText() << endl;
    }
    catch(...){
        throw;
    }

    return 0;
}
