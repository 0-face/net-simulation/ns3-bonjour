#include "PocoDnssdApp.h"

#include "Poco/Delegate.h"

using namespace std;
using namespace Poco::DNSSD;

class PocoAnnounceApp: public PocoDnssdApp{
public:
    using super = PocoDnssdApp;

public:
    bool parseArgs(int argc, const char ** argv) override{
        if (argc < 4) {
            cout << "Usage: " << argv[0]
                 << " <serviceName> <serviceType> <port>" << endl;
            cout << endl;
            cout << "Where:" << endl
                 << "\t" "serviceName: name of service to announce (ex.: \"My service\")\n"
                 << "\t" "serviceType: type of service (ex.: _http._tcp)" "\n"
                 << "\t" "port: port of service (ex.: 8080)" "\n"
                 << endl;
            return false;
        }

        serviceName = std::string(argv[1]);
        serviceType = std::string(argv[2]);
        servicePort = std::atoi(argv[3]);

        if(servicePort <= 0){
            cerr << "Invalid port value: " << argv[3] << endl;
            return false;
        }

        return true;
    }

    int run() override{
        try {
            initialize();
            announceService();
            waitFinish();
            uninitialize();
        } catch (const Poco::Exception & e) {
            cerr << e.displayText() << endl;
        }

        return 0;
    }

    void announceService(){
        Service service(0 /*interfaceIndex*/,
                serviceName,
                {} /*fullname*/,
                serviceType,
                {} /*domain*/,
                {} /*host*/,
                servicePort);

        cout << "Announcing service: ";
        cout << service << endl;

        dnssdResponder->serviceRegistered += Poco::delegate(onServiceRegistered);
        dnssdResponder->serviceRegistrationFailed += Poco::delegate(onError);

        serviceHandle = dnssdResponder->registerService(service);
    }

    void uninitialize() override{
        cout << "Unregistering service" << endl;
        if (!serviceHandle.isValid()) {
            dnssdResponder->unregisterService(serviceHandle);
        }

        super::uninitialize();
    }

    static void onServiceRegistered(
            const void* sender, const DNSSDResponder::ServiceEventArgs& args)
    {
        std::cout << "service registered: " << args.service << endl;
//        std::cout << "handle is valid: " << args.serviceHandle.isValid() << endl;
    }
    static void onError(const void* sender, const DNSSDResponder::ErrorEventArgs& args)
    {
        std::cerr
            << "Service registration failed: "
            << args.error.message()
            << " (" << args.error.code() << ")"
            << std::endl;
    }
protected:
    ServiceHandle serviceHandle;

    std::string serviceName, serviceType;
    int servicePort = 0;
};

int main(int argc, const char ** argv){
    try{
        PocoAnnounceApp app;
        app.main(argc, argv);
    }
    catch(const Poco::Exception & e){
        cerr << e.displayText() << endl;
    }
    catch(...){
        throw;
    }

    return 0;
}
