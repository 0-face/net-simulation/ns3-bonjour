#pragma once

#include "Poco/DNSSD/DNSSDResponder.h"
#include "Poco/DNSSD/DNSSDBrowser.h"

#ifdef USE_AVAHI
#include "Poco/DNSSD/Avahi/Avahi.h"
#else
#include "Poco/DNSSD/Bonjour/Bonjour.h"
#endif

#include <iostream>
#include <memory>
#include <signal.h>
#include <functional>

using namespace std;
using namespace Poco::DNSSD;

std::ostream & operator<<(std::ostream & out, const Service & s){
    out << "("
        << s.name() << ","
        << s.type() << ","
//        << s.port()
        << ")";

    return out;
}

std::vector<std::function<void(int)>> handlers;

void signalHandler(int sig){
    signal(sig, SIG_IGN);

    for(auto & handle : handlers){
        handle(sig);
    }
}

class PocoDnssdApp{
public:
    virtual int main(int argc, const char ** argv){
        if (!parseArgs(argc, argv)) {
            return -1;
        }
        return run();
    }

    virtual bool parseArgs(int argc, const char ** argv)=0;

    virtual int run()=0;

    virtual void initialize(){
        cout << "Initializing DNSSD implementation" << endl;
        Poco::DNSSD::initializeDNSSD();

        cout << "Starting DNS-SD responder" << endl;
        dnssdResponder = std::make_shared<DNSSDResponder>();
        dnssdResponder->start();
    }

    virtual void uninitialize(){
        cout << "Uninitializing DNSSD implementation" << endl;
        Poco::DNSSD::uninitializeDNSSD();
    }

    virtual void waitFinish(){
        cout << "Press Ctrl+C to finish" << endl;
        waitCtrl_C();
    }

    virtual void waitCtrl_C()
    {
        volatile bool finish=false;

        handlers.push_back([&finish](int /*sig*/){
            finish = true;
        });

        signal(SIGINT, signalHandler);

        while(!finish){
            sleep(1);
        }
    }

protected:
    std::shared_ptr<Poco::DNSSD::DNSSDResponder> dnssdResponder;
};
