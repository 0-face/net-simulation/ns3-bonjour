#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dependencies import DependenciesManager

def main():
    deps = DependenciesManager()

    deps.remove_dependencies()

if __name__ == "__main__":
    main()
