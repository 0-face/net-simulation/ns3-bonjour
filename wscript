#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path

def load_tools(ctx):
    ctx.load('compiler_cxx')

def options(ctx):
    load_tools(ctx)

    ctx.add_option('-r', '--run', action='store', dest='run',
        help='Execute a build target')

    # Not working yet
    #ctx.add_option('-d', '--debug', action='store_true', default=False,
    #    help='Run target using gdb')

    ctx.add_option('--use-avahi', action='store_true', default=False, dest='use_avahi',
      help='If set then use avahi')

def configure(ctx):
    load_tools(ctx)

    # Prefer search relative to the build dir to avoid possible mismatches with file in 'test_package' dir
    ctx.load('conanbuildinfo_waf', tooldir=[path.join(ctx.bldnode.abspath(), ".."), "."]);

    ctx.env.CXXFLAGS = ['-std=c++11', '-g3']

    ctx.env.use_avahi = ctx.options.use_avahi

def build(bld):
    for dce_script in ['bonjour-hello', 'bonjour-network', 'dce-poco-bonjour']:
        bld.program(
            target   = dce_script,
            source   = path.join('dce-scripts', dce_script + ".cpp"),
            includes = ['dce-scripts'],
            use      = bld.env.conan_dependencies)

    dnssd_implementation = 'PocoDNSSD_Avahi' if bld.env.use_avahi else 'PocoDNSSD_Bonjour'

    for p in ['poco-announce', 'poco-search']:
        bld.program(
            target   = p,
            source   = path.join('programs', p + ".cpp"),
            includes = ['programs'],
            cxxflags  = ['-fPIC'],
            linkflags = ['-pie', '-rdynamic'],
            defines   = ['USE_AVAHI'] if bld.env.use_avahi else [],
            use       = ['PocoDNSSD', dnssd_implementation, 'PocoDNSSD_core', 'Poco', 'bonjour'])

    if bld.options.run:
        bld(
            source = bld.options.run,
            always = True,
            rule = run_script
        )

################################### Helpers ################################################

def run_script(task):
    ctx = task.generator.bld
    executable = task.inputs[0].abspath()
    log_path   = make_log_path(ctx, executable)

    # Not working yet
    #if ctx.options.debug:
    #    executable = "gdb %s -ex run" % executable

    return task.exec_command(executable, env=make_env(ctx), cwd=log_path)

def make_env(ctx):
    build_path = ctx.bldnode.abspath()

    DCE_PATHs = [
        build_path,
        path.join(build_path, 'programs'),
        path.join(ctx.srcnode.abspath(), 'bin')
    ]
    DCE_PATHs.extend(ctx.env.LIBPATH_net_next_nuse)
    DCE_PATHs.extend(ctx.env.LIBPATH_bonjour)

    bin_dce = path.join(ctx.env.LIBPATH_ns3_dce[0], '..', 'bin')
    DCE_PATHs.append(bin_dce)

    env = {}
    env['DCE_PATH'] = ':'.join(DCE_PATHs)
    env['PATH'] = os.environ['PATH'] + ':' + ':'.join(ctx.env.LIBPATH_ns3_dce)
    env['LD_LIBRARY_PATH'] = ':'.join(ctx.env.LIBPATH_bonjour)

    return env

def make_log_path(ctx, executable):
    basename = os.path.basename(executable)

    log_path = path.join(ctx.bldnode.abspath(), 'log', basename)
    _try_create_dir(log_path)

    return log_path

def _try_create_dir(dir_path):
    import os
    try:
        os.makedirs(dir_path)
    except OSError:
        if not os.path.isdir(dir_path):
            raise
