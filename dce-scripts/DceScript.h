#pragma once

#include "ns3/core-module.h"
#include "ns3/dce-module.h"

#include "ns3/internet-module.h"
#include "ns3/application-container.h"

#include <iostream>

class DceScript{
public:
    virtual ~DceScript(){}

    void run(int argc, char ** argv){
        if(!parseArgs(argc, argv)){
            return;
        }

        setup();
        runSimulation();
    }

protected:
    virtual bool parseArgs(int argc, char ** argv){
        ns3::CommandLine cmd;
        cmd.AddValue("stack",
                     "Name of IP stack: ns3/linux.",
                     stack);

        cmd.AddValue("duration",
                     "Total simulation time (in seconds)",
                     this->durationInSeconds);

        cmd.Parse (argc, argv);

        return true;
    }

    virtual void setup(){
        createNodes();
        configureStack();
        configureNetwork();
        populateRoutingTables();

        configureApplications();
    }

    virtual void runSimulation(){
        std::cout << "Starting simulation (for " << durationInSeconds << " seconds)" << std::endl;

        ns3::Simulator::Stop (ns3::Seconds(this->durationInSeconds));
        ns3::Simulator::Run ();
        ns3::Simulator::Destroy ();

        std::cout << "Finished simulation" << std::endl;
    }

protected:
    virtual void createNodes(){
        std::cout << "Creating " << numberOfNodes << " nodes" << std::endl;
        nodes.Create(numberOfNodes);
    }

    virtual void configureStack(){
        if (stack == "ns3"){
            ns3::InternetStackHelper stack;
            stack.Install (nodes);
            dceManager.Install (nodes);
        }
        else if (stack == "linux"){
            dceManager.SetNetworkStack ("ns3::LinuxSocketFdFactory",
                                        "Library", ns3::StringValue ("liblinux.so"));
            dceManager.Install (nodes);

            ns3::LinuxStackHelper stack;
            stack.Install (nodes);
        }
    }


    virtual void populateRoutingTables()
    {
        ns3::Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
        if (stack == "linux"){
            ns3::LinuxStackHelper::PopulateRoutingTables ();
        }
    }

    virtual void configureNetwork()
    {}

    virtual void configureApplications()
    {}

protected:
    virtual ns3::ApplicationContainer addApplication(
            ns3::Ptr<ns3::Node> node,
            std::string executable,
            std::string args={},
            float startSeconds=-1,
            uint32_t stackSize = 1<<20)
    {
        std::cout << "Node(" << node->GetId() << ") "
             << "Add application: '" << executable;

        if(!args.empty()){
            std::cout << " " << args;
        }
        std::cout << "'";

        if(startSeconds >= 0.0){
            std::cout << " starting at " << startSeconds << "s";
        }

        std::cout << std::endl;

        appHelper.ResetArguments();
        appHelper.SetStackSize(stackSize);
        appHelper.SetBinary(std::move(executable));

        if(!args.empty()){
            appHelper.ParseArguments(std::move(args));
        }

        auto app = appHelper.Install(node);

        if(startSeconds >= 0.0){
            app.Start(ns3::Seconds(startSeconds));
        }

        return app;
    }
protected: //options
    double durationInSeconds = 30.0;
    std::string stack="linux";

protected: //Dce variables
    unsigned numberOfNodes = 1;
    ns3::NodeContainer nodes;
    ns3::DceManagerHelper dceManager;
    ns3::DceApplicationHelper appHelper;
};
