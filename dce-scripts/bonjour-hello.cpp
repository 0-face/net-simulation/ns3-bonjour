#include "DceScript.h"

#include <iostream>

using namespace ns3;
using namespace std;

class BonjourBasic: public DceScript{
protected:
    void configureApplications() override{
        const char * announceService = "-R \"MyTest\" _http._tcp . 8081";
        const char * browseService   = "-B _http._tcp";

        addApplication(nodes.Get(0), "mdnsd" , "-debug"       , 2.0);
        addApplication(nodes.Get(0), "dns-sd", announceService, 7.0);
        addApplication(nodes.Get(0), "dns-sd", browseService  , 12.0);
    }
};

int main (int argc, char ** argv){
    BonjourBasic script;
    script.run(argc, argv);

    return 0;
}
