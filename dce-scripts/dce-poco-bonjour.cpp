#include "DceScript.h"
#include "ns3/csma-module.h"

#include <iostream>

using namespace ns3;
using namespace std;

class PocoBonjourScript: public DceScript{
public:
    PocoBonjourScript(){
        this->numberOfNodes = 2;
    }

    void configureApplications() override{
        const char * announceService = "\"MyServiceName\" _http._tcp 8081";
        const char * browseService   = "_http._tcp";

        addApplication(nodes.Get(0), "mdnsd" , "-debug"       , 2.0);
        addApplication(nodes.Get(1), "mdnsd" , "-debug"       , 2.0);
        addApplication(nodes.Get(0), "poco-announce", announceService, 7.0);
        addApplication(nodes.Get(1), "poco-search"  , browseService  , 12.0);
    }

    void configureNetwork() override{
        CsmaHelper csma;
        csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
        csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

        NetDeviceContainer csmaDevices = csma.Install (nodes);

        Ipv4AddressHelper address;
        address.SetBase ("10.1.1.0", "255.255.255.0");

        Ipv4InterfaceContainer csmaInterfaces = address.Assign (csmaDevices);
    }
};

int main (int argc, char ** argv){
    PocoBonjourScript script;
    script.run(argc, argv);

    return 0;
}
