#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
import os
from os import path

class Recipe(ConanFile):
    settings = "os", "compiler", "arch"

    generators = "Waf", "virtualrunenv"
    exports = "*.cpp", "wscript"

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.1.1@noface/stable"
    )

    requires = (
        "ns3-dce/1.10@noface/stable",
        "bonjour/878.30.4@noface/stable",

        "PocoDNSSD/0.1@noface/dce",
        ("PocoDNSSD-Bonjour/0.1@noface/dce", "override"),
        ("PocoDNSSD-Avahi/0.1@noface/dce"  , "override"),
        ("PocoDNSSD-core/0.1@noface/dce"   , "override"),
        ("Poco/1.7.9@noface/dce"           , "override")
    )

    default_options = (
        'bonjour:pie=True',
        'bonjour:use_soname=True',
        "PocoDNSSD:use_avahi=True",
        "PocoDNSSD:use_bonjour=True"
    )

    def configure(self):
        self.options['bonjour'].extra_cflags='-U_FORTIFY_SOURCE -DUSE_TCP_LOOPBACK -DHAVE_IPV6=0 -g'

    def imports(self):
        self.copy("mdnsd", src="bin", dst="bin")
        self.copy("dns-sd", src="bin", dst="bin")

    def build(self):
        cmd = 'waf -v configure build -o "{}"'.format(path.join(self.build_folder, 'build'))

        self.output.info("Running cmd '{}' in '{}'".format(cmd, self.source_folder))
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        DCE_PATHs = [path.join(self.build_folder, 'build'), path.join(self.source_folder, 'bin')]

        env = {}
        env['DCE_PATH'] = ':'.join(DCE_PATHs)

        self.output.info("running [cwd= {}]:\n\t{}".format(cwd, example_cmd))
        with tools.environment_append(env):
            self.run(path.join(self.build_folder, 'build', 'bonjour-hello'))
